import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ModalContainer, ModalRoute } from 'react-router-modal';
import 'react-router-modal/css/react-router-modal.css';
import Home from '../home/Home';
import Header from '../header/Header';
import Footer from '../footer/Footer';
import Login from '../login-form/Login';
import NewsList from '../news-list/NewsList';
import TutorialList from '../tutorial-list/TutorialList';
import ProjectList from '../project-list/ProjectList';
import UserList from '../user-list/UserList';
import CreateArticle from '../news-list/CreateArticle';
import './styles/App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/news" render={() => <Redirect to="/news/new/1" />} />
          <Route exact path="/news/new/:page" component={NewsList} />
          <ModalRoute exact path="/news/create" parentPath="/news" component={CreateArticle} />
          <Route exact path="/tutorials" component={TutorialList} />
          <Route exact path="/projects" component={ProjectList} />
          <Route exact path="/users" component={UserList} />
          <Route exact path="/login" component={Login} />
        </Switch>
        <Footer />
        <ModalContainer />
      </div>
    );
  }
}

export default App;
