import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { Menu, Button } from 'semantic-ui-react';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../../constants';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { activeItem: 'home' };
  }
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    // whitespace
    const whitespaceStyle = {
      padding: '0px'
    };
    const { activeItem } = this.state;
    const userId = localStorage.getItem(GC_USER_ID);
    return (
      <div>
        <Menu pointing secondary size="massive" color={'red'}>
          <Link to="/">
            <Menu.Item as="p" name="home" active={activeItem === 'home'} onClick={this.handleItemClick}>
              Acalanes Startup Club
            </Menu.Item>
          </Link>
          <Link to="/news">
            <Menu.Item as="p" name="news" active={activeItem === 'news'} onClick={this.handleItemClick}>
              News
            </Menu.Item>
          </Link>
          <Link to="/tutorials">
            <Menu.Item as="p" name="tutorials" active={activeItem === 'tutorials'} onClick={this.handleItemClick}>
              Tutorials
            </Menu.Item>
          </Link>
          <Link to="/projects">
            <Menu.Item as="p" name="projects" active={activeItem === 'projects'} onClick={this.handleItemClick}>
              Projects
            </Menu.Item>
          </Link>
          <Menu.Menu position="right">
            {userId ?
              <Menu.Item as="p" name="logout" active={activeItem === 'logout'} onClick={this.handleItemClick}>
                <Button
                  basic
                  color="red"
                  onClick={() => {
                    localStorage.removeItem(GC_USER_ID);
                    localStorage.removeItem(GC_AUTH_TOKEN);
                    this.props.history.push('/');
                  }}
                >Logout</Button>
              </Menu.Item>
              :
              <Link to="/login">
                <Menu.Item name="login" active={activeItem === 'login'} onClick={this.handleItemClick}>
                  <Button
                    basic
                    color="green"
                  >
                    Login
                  </Button>
                </Menu.Item>
              </Link>
            }
          </Menu.Menu>
        </Menu>
        <div className="whitespace" style={whitespaceStyle} />
      </div>
    );
  }
}

export default withRouter(Header);
