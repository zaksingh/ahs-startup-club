import React, { Component } from 'react';
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Segment,
  Card
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import goldenBridge from './golden-gate-bridge-noBlur.jpg';

export default class Home extends Component {

  render() {

    return (
      <div >
        <img src={goldenBridge} alt="boohoo" />
        <Segment
          inverted
          textAlign="center"
          style={{ minHeight: 700, padding: '1em 0em' }}
          vertical
        >

          <Container text>
            <Header
              as="h1"
              color={'red'}
              content="<AHS STARTUP CLUB>"
              style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: 0, marginTop: '3em' }}
            />
            <Header
              as="h2"
              content="Meetings Wednesday at lunch in Room 104"
              inverted
              style={{ fontSize: '1.7em', fontWeight: 'normal' }}
            />
            <Divider />
            <Link to="/login">
              <Button primary size="huge">
                  Join
                <Icon name="right arrow" />
              </Button>
            </Link>
          </Container>
        </Segment>
        <Segment style={{ padding: '5em 0em' }} vertical>
          <Grid container stackable >
            <Grid.Row>
              <Grid.Column width={4}>
                <Header as="h3" style={{ fontSize: '2em' }}>Follow your dreams with us.</Header>
                <p style={{ fontSize: '1.33em' }}>
                  We will help you realize your idea through a series of coding bootcamps and design
                  sessions.
                </p>
                <Header as="h3" style={{ fontSize: '2em' }}>Anything is possible</Header>
                <p style={{ fontSize: '1.33em' }}>
                  So inspirational, even Mr. Appel is proud of us.
                </p>
              </Grid.Column>
              <Grid.Column floated="right" width={10}>
                <Divider
                  as="h4"
                  className="header"
                  horizontal
                  style={{ margin: '3em 0em', textTransform: 'uppercase' }}
                >
                  <a href="#">FOUNDERS</a>
                </Divider>
                <Card.Group>
                  <Card centered>
                    <Image src="" alt="No image yet" />
                    <Card.Content>
                      <Card.Header>
                    Zak Singh
                      </Card.Header>
                      <Card.Meta>
                        <span className="role">
                      PRESIDENT
                        </span>
                      </Card.Meta>
                      <Card.Description>
                    the guy who made this website
                      </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                      <a>
                        <Icon name="user" />
                    zaksingh@comcast.net
                      </a>
                    </Card.Content>
                  </Card>
                  <Card centered>
                    <Image src="" alt="No image yet" />
                    <Card.Content>
                      <Card.Header>
                    Jonli Keshavarz
                      </Card.Header>
                      <Card.Meta>
                        <span className="role">
                      VICE PRESIDENT
                        </span>
                      </Card.Meta>
                      <Card.Description>
                    does stuff too
                      </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                      <a>
                        <Icon name="user" />
                    jonlikeshavarz@whatever.com
                      </a>
                    </Card.Content>
                  </Card>
                </Card.Group>
                <Divider
                  as="h4"
                  className="header"
                  horizontal
                  style={{ margin: '3em 0em', textTransform: 'uppercase' }}
                >
                  <Link to="/users">
                    <Button> FULL ROSTER </Button>
                  </Link>
                </Divider>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </div>
    );
  }
}
