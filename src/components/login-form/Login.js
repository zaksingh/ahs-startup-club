import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { gql, graphql, compose } from 'react-apollo';
import { Button, Form, Grid, Header, Image, Input, Message, Segment, Divider } from 'semantic-ui-react';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../../constants';

class Login extends Component {

  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    name: '',
    imageUrl: '',
    imageId: ''
  }

  componentWillMount() {
    document.body.classList.add('login');
  }

  componentWillUnmount() {
    document.body.classList.remove('login');
  }

  onDrop = (files) => {
    // prepare form data, use data key!
    const data = new FormData();
    data.append('data', files[ 0 ]);

    // use the file endpoint
    fetch('https://api.graph.cool/file/v1/cj5o7hxclp5b30122zleg4ez9', {
      method: 'POST',
      body: data
    }).then(response => response.json()).then((image) => {
      this.setState({
        imageId: image.id,
        imageUrl: image.url,
      });
    });
  }

  _confirm = async () => {
    const { name, email, password, imageId } = this.state;
    if (this.state.login) {
      const result = await this.props.signinUserMutation({
        variables: {
          email,
          password
        }
      });
      const id = result.data.signinUser.user.id;
      const token = result.data.signinUser.token;
      this._saveUserData(id, token);
    } else {
      const result = await this.props.createUserMutation({
        variables: {
          name,
          email,
          password,
          imageId
        }
      });
      const id = result.data.signinUser.user.id;
      const token = result.data.signinUser.token;
      this._saveUserData(id, token);
    }
    this.props.history.push('/');
  }

  _saveUserData = (id, token) => {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
  }

  render() {
    return (
      <Grid
        textAlign="center"
        style={{ height: '100%' }}
        verticalAlign="middle"
        padded
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" color="blue" textAlign="center">
            {this.state.login ? 'Login' : 'Sign up'}
          </Header>
          <Form size="large">
            <Segment stacked>
              {!this.state.login &&
              <Form.Input
                fluid
                value={this.state.name}
                onChange={e => this.setState({ name: e.target.value })}
                icon="id card"
                type="text"
                iconPosition="left"
                placeholder="Your name"
              />}
              <Form.Input
                fluid
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value })}
                icon="user"
                type="text"
                iconPosition="left"
                placeholder="E-mail address"
              />
              <Form.Input
                fluid
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value })}
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
              />
              {!this.state.login &&
              <Dropzone
                className="dropzone"
                onDrop={this.onDrop}
                accept="image/*"
                multiple={false}
              >
                {!this.state.imageUrl ?
                  <Button
                    color="green"
                    fluid
                    size="large"
                  >
                  Upload a photo of yourself
                  </Button>
                  :
                  <Button
                    disabled
                    fluid
                    size="large"
                  >
                  File received
                  </Button>}
              </Dropzone>}
              <Divider />
              <Button
                color="blue"
                fluid
                size="large"
                onClick={() => this._confirm()}
              >
                {this.state.login ? 'login' : 'create account' }
              </Button>
              <Divider />
              <Button
                fluid
                size="large"
                onClick={() => this.setState({ login: !this.state.login })}
              >
                {this.state.login ? 'need to create an account?' : 'already have an account?' }
              </Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

// mutations
const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation($name: String!, $email: String!, $password: String!, $imageId: ID!) {
    createUser(
      name: $name,
      imageId: $imageId,
      authProvider: {
        email: {
          email: $email,
          password: $password
        }
      }
    ) {
      id
      image {
        url
      }
    }

    signinUser(email: {
      email: $email,
      password: $password
    }) {
      token
      user {
        id
      }
    }
  }
`;

const SIGNIN_USER_MUTATION = gql`
  mutation SigninUserMutation($email: String!, $password: String!) {
    signinUser(email: {
      email: $email,
      password: $password
    }) {
      token
      user {
        id
      }
    }
  }
`;

export default compose(
  graphql(CREATE_USER_MUTATION, { name: 'createUserMutation' }),
  graphql(SIGNIN_USER_MUTATION, { name: 'signinUserMutation' })
)(Login);
