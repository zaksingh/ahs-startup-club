import React, { Component } from 'react';
import { Item } from 'semantic-ui-react';

class Article extends Component {
  render() {
    return (
      <Item>
        <Item.Image src={this.props.article.user.image.url} />
        <Item.Content verticalAlign="middle">
          <Item.Header as="a" href={this.props.article.url}>{this.props.article.title}</Item.Header>
          <Item.Meta>Description</Item.Meta>
          <Item.Description>{this.props.article.description}</Item.Description>
          <Item.Extra>Posted by {this.props.article.user.name}</Item.Extra>
        </Item.Content>
      </Item>
    );
  }
}

export default Article;
