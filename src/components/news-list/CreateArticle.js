import React, { Component } from 'react';
import { Form, Header, Grid } from 'semantic-ui-react';
import { graphql, gql } from 'react-apollo';
import { GC_USER_ID, ARTICLES_PER_PAGE } from '../../constants';
import { ALL_ARTICLES_QUERY } from './NewsList';

class CreateArticle extends Component {
  state = {
    title: '',
    description: '',
    url: ''
  }

  _createArticle = async () => {
    const user = localStorage.getItem(GC_USER_ID);
    if (!user) {
      console.error('No user logged in');
      return;
    }
    const { title, description, url } = this.state;
    await this.props.createArticleMutation({
      variables: {
        title,
        description,
        url,
        user
      },
      update: (store, { data: { createArticle } }) => {
        const first = ARTICLES_PER_PAGE;
        const skip = 0;
        const orderBy = 'createdAt_DESC';
        const data = store.readQuery({
          query: ALL_ARTICLES_QUERY,
          variables: { first, skip, orderBy }
        });
        data.allArticles.splice(0, 0, createArticle);
        data.allArticles.pop();
        store.writeQuery({
          query: ALL_ARTICLES_QUERY,
          data,
          variables: { first, skip, orderBy }
        });
      }
    });
    // this.props.history.push('/news');
  }

  render() {
    return (
      <Grid
        textAlign="center"
        verticalAlign="middle"
        style={{ height: '100%' }}
        padded
      >
        <Grid.Column style={{ maxWidth: 1000 }}>
          <Header as="h2" color="red" textAlign="center">
            Post an article
          </Header>
          <Form>
            <Form.Group widths="equal">
              <Form.Input
                label="Title"
                placeholder="Title of the article"
                type="text"
                value={this.state.title}
                onChange={e => this.setState({ title: e.target.value })}
              />
              <Form.Input
                label="Url"
                placeholder="Link to the article"
                type="text"
                value={this.state.url}
                onChange={e => this.setState({ url: e.target.value })}
              />
            </Form.Group>
            <Form.TextArea
              label="Description"
              placeholder="Tell us a little about the article."
              type="text"
              value={this.state.description}
              onChange={e => this.setState({ description: e.target.value })}
            />
            <Form.Button
              onClick={() => this._createArticle()}
            >
              Submit Article
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

// mutations

const CREATE_ARTICLE_MUTATION = gql`
  mutation CreateArticleMutation($title: String!, $description: String!, $url: String!, $user: ID!) {
    createArticle(
      title: $title,
      description: $description,
      url: $url,
      userId: $user
    ) {
      id
      createdAt
      url
      title
      description
      user {
        id
        name
        image {
          id
          url
        }
      }
    }
  }
`;

export default graphql(CREATE_ARTICLE_MUTATION, { name: 'createArticleMutation' })(CreateArticle);
