import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ModalLink } from 'react-router-modal';
import { graphql, gql } from 'react-apollo';
import { Loader, Button, Divider, Item, Grid, Header, Segment } from 'semantic-ui-react';
import { GC_USER_ID, ARTICLES_PER_PAGE } from '../../constants';
import Article from './Article';
import CreateArticle from './CreateArticle';

class NewsList extends Component {

  _getArticlesToRender = (isNewPage) => {
    if (isNewPage) {
      return this.props.allArticlesQuery.allArticles;
    }
    return this.props.allArticlesQuery.allArticles;
  }

  _nextPage = () => {
    const page = parseInt(this.props.match.params.page, 10);
    if (page <= this.props.allArticlesQuery._allArticlesMeta.count / ARTICLES_PER_PAGE) {
      const nextPage = page + 1;
      this.props.history.push(`/news/new/${ nextPage }`);
    }
  }

  _previousPage = () => {
    const page = parseInt(this.props.match.params.page, 10);
    if (page > 1) {
      const nextPage = page - 1;
      this.props.history.push(`/news/new/${ nextPage }`);
    }
  }

  render() {
    const loaderPadding = {
      padding: '100px',
      marginBottom: '40%'
    };
    // catches
    if (this.props.allArticlesQuery && this.props.allArticlesQuery.loading) {
      return (
        <div style={loaderPadding}>
          <Loader active inline="centered"> <h3 color={'red'}>LOADING</h3> </Loader>
        </div>
      );
    }

    if (this.props.allArticlesQuery && this.props.allArticlesQuery.error) {
      return (
        <div> Error fetching articles. </div>
      );
    }

    const isNewPage = this.props.location.pathname.includes('news/new');
    const articlesToRender = this.props.allArticlesQuery.allArticles;
    const user = localStorage.getItem(GC_USER_ID);

    return (
      <Grid
        textAlign="left"
        verticalAlign="middle"
        padded
      >
        <Grid.Column>
          <Header
            as="h1"
            color={'red'}
            textAlign="center"
            content="<ARTICLES>"
            style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: 0 }}
            subheader={'and discussion'}
          />
          {user ?
            <ModalLink path="/news/create" parentPath="/news">
              <Button>
                  New Post
              </Button>
            </ModalLink>
            :
            <Link to="/login">
              <Button>
                Log in to make a post
              </Button>
            </Link>
          }
          <Divider />
          <Segment>
            <Item.Group divided>
              {articlesToRender.map(article => (
                <Article key={article.id} article={article} />
              ))}
            </Item.Group>
          </Segment>
          {isNewPage &&
          <div>
            <Button onClick={() => this._previousPage()}>Previous</Button>
            <Button onClick={() => this._nextPage()}>Next</Button>
          </div>
          }
        </Grid.Column>
      </Grid>
    );
  }
}

// queries
export const ALL_ARTICLES_QUERY = gql`
  query AllArticlesQuery($first: Int, $skip: Int, $orderBy: ArticleOrderBy) {
    allArticles(first: $first, skip: $skip, orderBy: $orderBy) {
      id
      createdAt
      url
      description
      title
      user {
        id
        name
        image {
          url
        }
      }
    }
    _allArticlesMeta {
      count
    }
  }
`;

export default graphql(ALL_ARTICLES_QUERY, {
  name: 'allArticlesQuery',
  options: (ownProps) => {
    const page = parseInt(ownProps.match.params.page, 10);
    const isNewPage = ownProps.location.pathname.includes('news/new');
    const skip = isNewPage ? (page - 1) * ARTICLES_PER_PAGE : 0;
    const first = isNewPage ? ARTICLES_PER_PAGE : 100;
    const orderBy = isNewPage ? 'createdAt_DESC' : null;
    return {
      variables: { first, skip, orderBy }
    };
  }
})(NewsList);
