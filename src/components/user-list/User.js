import React, { Component } from 'react';
import { Card, Image } from 'semantic-ui-react';

export default class User extends Component {

  render() {
    return (
      <Card>
        <Image src={this.props.user.image.url} />
        <Card.Content>
          <Card.Header>
            {this.props.user.name}
          </Card.Header>
          <Card.Meta>
            <span className="email">
              {this.props.user.email}
            </span>
          </Card.Meta>
          <Card.Description>
            {this.props.user.bio}
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}
