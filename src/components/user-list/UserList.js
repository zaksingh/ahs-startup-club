import React, { Component } from 'react';
import { Card, Loader, Grid, Header, Segment } from 'semantic-ui-react';
import { graphql, gql } from 'react-apollo';
import User from './User';

class UserList extends Component {
  render() {
    // catches
    if (this.props.allUsersQuery && this.props.allUsersQuery.loading) {
      return (
        <Loader active inline="centered" />
      );
    }

    if (this.props.allUsersQuery && this.props.allUsersQuery.error) {
      return (
        <div> Error fetching users. </div>
      );
    }

    const usersToRender = this.props.allUsersQuery.allUsers;

    return (
      <Grid
        textAlign="center"
        verticalAlign="middle"
        padded
      >
        <Grid.Column>
          <Header as="h2" color="blue" textAlign="center">
            Members
          </Header>
          <Segment textAlign="center">
            <Card.Group itemsPerRow={3}>
              {usersToRender.map(user => (
                <User key={user.id} user={user} />
              ))}
            </Card.Group>
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

const ALL_USERS_QUERY = gql`
  query AllUsersQuery {
    allUsers {
      id
      email
      name
      bio
      image {
        id
        url
      }
    }
  }
`;

export default graphql(ALL_USERS_QUERY, { name: 'allUsersQuery' })(UserList);
