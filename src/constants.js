// for authentication
export const GC_USER_ID = 'graphcool-user-id';
export const GC_AUTH_TOKEN = 'graphcool-auth-token';

// news
export const ARTICLES_PER_PAGE = 5;
